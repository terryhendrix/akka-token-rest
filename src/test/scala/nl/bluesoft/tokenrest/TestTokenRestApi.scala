package nl.bluesoft.tokenrest

import scala.concurrent.Future
import spray.routing._

trait TestTokenRestApi extends TokenRestApi
{
  override def restServiceName: String = "test"
  override implicit val ec = system.dispatcher
  override def privateRoute = { username =>
    pathPrefix("test") {
      complete {
        s"Secret stuff for $username"
      }
    }
  }

  override def viewRoute = {
    get{
      complete {
        "Hello world!"
      }
    }
  }
  def authorize:(String, String) => Future[Boolean] = {
    (username, password) => Future{ username == "terry" && password == "1234"}
  }
}