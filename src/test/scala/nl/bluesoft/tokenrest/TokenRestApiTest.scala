package nl.bluesoft.tokenrest

import akka.testkit.TestKit
import akka.actor.ActorSystem
import org.scalatest.FlatSpecLike
import scala.concurrent.Await
import scala.concurrent.duration._
import akka.event.{Logging, LoggingAdapter}

/**
 * Created by terryhendrix on 09/11/14.
 */
class TokenRestApiTest extends TestKit(ActorSystem("TestSystem")) with FlatSpecLike with TestTokenRestApi
{
  override def log: LoggingAdapter = Logging(system, "Testing")

  "An TokenRestApi" should "be started" in {
    Thread.sleep(600000)
  }
}