package nl.bluesoft.tokenrest
import scala.concurrent.duration._
import akka.actor._
import java.util.UUID
import scala.concurrent.Future
import scala.Some
import nl.bluesoft.tokenrest.crypto.AES
import org.apache.commons.codec.binary.Base64
import nl.bluesoft.tokenrest.MessagingV1.LoginFailed

/**
 * Created by terryhendrix on 14/11/14.
 */
class LoginWorker(val authorized:(String, String) => Future[Boolean]) extends Actor with ActorLogging
                                                                      with SessionRegion with EncryptionKey
{
  implicit def uuidToString(uuid:UUID) = uuid.toString
  override val system = context.system
  implicit val ec = system.dispatcher
  var ref:Option[ActorRef] = None
  context.setReceiveTimeout(5 seconds)

  override def receive: Actor.Receive = {
    case MessagingV1.Login(username, password, ip) =>
      ref = Option(sender())
      login(username, password) {
        case true =>
          val sessionId = UUID.randomUUID()
          sessionRegion ! MessagingV1.CreateSession(sessionId, username, ip)

          val encryptedSessionId = Base64.encodeBase64(AES.encrypt(sessionId.getBytes, encryptionKey))
          ref map { _ ! MessagingV1.LoginSuccessful(new String(encryptedSessionId)) }
        case false =>
          log.error(s"Login for user $username failed! Call originated from $ip")
          ref map ( _ ! MessagingV1.LoginFailed)
      } recover {
        case ex:Throwable =>
          ex.printStackTrace()
          ref map ( _ ! MessagingV1.LoginFailed)
      }
    case ReceiveTimeout => self ! PoisonPill
  }

  def login(username:String, password:String)(pf:PartialFunction[Boolean, Unit]) = {
    authorized(username,password) map pf
  }
}
