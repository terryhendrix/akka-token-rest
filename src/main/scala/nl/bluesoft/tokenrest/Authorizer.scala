package nl.bluesoft.tokenrest
import scala.concurrent.duration._
import akka.actor.{ActorRef, PoisonPill, ReceiveTimeout, FSM}
import nl.bluesoft.tokenrest.crypto.AES
import org.apache.commons.codec.binary.Base64
import scala.util.Try

object Authorizer
{
  sealed trait State
  case object Authorizing extends State
  case object Authorized extends State
}

sealed case class AuthorizationData(ip:String)
import Authorizer._
class Authorizer(ip:String) extends FSM[State , AuthorizationData]
                            with SessionRegion with EncryptionKey
{
  implicit def actorRefToActorRefOption(ref:ActorRef) = Option(ref)
  override val system = context.system
  startWith(Authorizing, AuthorizationData(ip))
  context.setReceiveTimeout(3 seconds)
  var ref:Option[ActorRef] = None

  when(Authorizing) {
    case Event(msg:MessagingV1.HandleRequest, data:AuthorizationData) =>
      ref = sender()
      val tryDecode = Try(new String(AES.decrypt(Base64.decodeBase64(msg.sessionId), encryptionKey)))
      tryDecode.map { sessionId:String =>
        sessionRegion ! MessagingV1.CheckSession(sessionId, data.ip)
      } recover {
        case ex:Throwable =>
          log.error("Error while decoding", ex)
        self ! MessagingV1.InvalidSession
      }
      goto(Authorized)
  }

  when(Authorized) {
    case Event(msg:MessagingV1.BelongsToUser, data:AuthorizationData) =>
      ref map { _ ! msg }
      context.setReceiveTimeout(1 milli)
      stay()
    case Event(MessagingV1.InvalidSession, data:AuthorizationData) =>
      ref map { _ ! MessagingV1.InvalidSession}
      context.setReceiveTimeout(1 milli)
      stay()
  }

  whenUnhandled {
    case Event(ReceiveTimeout, _) =>
      self ! PoisonPill
      stay()
    case Event(any, _) =>
      log.info(s"Unhandled message $any in $stateName with $stateData")
      stay()
  }
}
