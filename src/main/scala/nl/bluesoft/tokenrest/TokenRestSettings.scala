package nl.bluesoft.tokenrest

import akka.actor.ActorSystem
import scala.util.Try
import scala.concurrent.duration.{Duration, FiniteDuration}
import java.util.concurrent.TimeUnit

/**
 * Created by terryhendrix on 15/11/14.
 */

object TokenRest{

  implicit def stringToFiniteDuration(s:String): FiniteDuration = {
    val d = Duration(s)
    FiniteDuration(d.toMillis, TimeUnit.MILLISECONDS)
  }

  def createSettings(system:ActorSystem, restServiceName: String):TokenRestSettings = {
    TokenRestSettings(
      system.settings.config.getString(s"rest.$restServiceName.interface"),
      system.settings.config.getInt(s"rest.$restServiceName.port"),
      system.settings.config.getString(s"rest.$restServiceName.timeout"),
      Try(Option(system.settings.config.getString(s"rest.$restServiceName.username"))).getOrElse(None),
      Try(Option(system.settings.config.getString(s"rest.$restServiceName.password"))).getOrElse(None),
      system.settings.config.getString(s"rest.$restServiceName.session-timeout")
    )
  }
}

case class TokenRestSettings(interface:String, port:Int, timeout:FiniteDuration, username:Option[String], password:Option[String], sessionTimeout:FiniteDuration){
  override def toString() = s"$interface:$port timeout='$timeout' sessionTimeout='${sessionTimeout.toSeconds} seconds'"
}