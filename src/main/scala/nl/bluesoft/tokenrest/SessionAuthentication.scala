package nl.bluesoft.tokenrest

import spray.routing._
import akka.actor.{ActorSystem, Props}
import java.util.UUID
import scala.concurrent.{ExecutionContext, Await}
import scala.util.Success
import spray.routing.Directives._
import akka.pattern.ask
import scala.concurrent.duration._
import akka.util.Timeout

/**
 * Created by terryhendrix on 15/11/14.
 */
trait SessionAuthentication {
  def system:ActorSystem
  implicit def timeout:Timeout
  implicit def ec:ExecutionContext

  def clientSession(sessionId: String, ip:String): Directive1[String] = {
    val worker = system.actorOf(Props(new Authorizer(ip)),s"Authorizer-${UUID.randomUUID()}")
    val f = worker ? MessagingV1.HandleRequest(sessionId)
    Await.result(f, 5 seconds) match {
      case MessagingV1.BelongsToUser(username)  => provide(username)
      case any            => throw new Exception("Invalid session")
    }
  }
}
