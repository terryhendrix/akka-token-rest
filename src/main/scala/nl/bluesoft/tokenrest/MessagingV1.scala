package nl.bluesoft.tokenrest

object MessagingV1
{
  //Session
  case class CreateSession(sessionId:String, username:String, ip:String)
  case class CheckSession(sessionId:String, ip:String)
  case class InvalidateSession(sessionId:String)
  case class BelongsToUser(username:String)
  case object Stop
  case object InvalidSession

  // LoginWorker
  case class Login(username:String, password:String, ip:String)
  case class LoginSuccessful(sessionId:String)
  case object LoginFailed

  // Authorizer
  case class HandleRequest(sessionId:String)
}
