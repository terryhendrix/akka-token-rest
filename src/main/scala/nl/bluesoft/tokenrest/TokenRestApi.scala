package nl.bluesoft.tokenrest
import java.text.SimpleDateFormat
import java.util.{Date, UUID}

import akka.actor.{ActorSystem, Props}
import akka.event.LoggingAdapter
import akka.pattern.ask
import akka.util.Timeout
import spray.http.{MediaTypes, RemoteAddress, StatusCodes}
import spray.httpx.SprayJsonSupport
import spray.httpx.marshalling.MetaMarshallers
import spray.json._
import spray.routing._
import spray.util.LoggingContext

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

trait ApiMessage
case class FailureApiResponse(success:Boolean = false, messages:List[String]) extends ApiMessage
case class LoginRequest(username:String, password:String) extends ApiMessage
case class LoginResponse(sessionId:Option[String],success:Boolean, errors:Option[List[String]]) extends ApiMessage

trait JsonMarshallers extends DefaultJsonProtocol with SprayJsonSupport with MetaMarshallers
{
  private[tokenrest] implicit val _apiTokenFailureResponse = jsonFormat2(FailureApiResponse)
  private[tokenrest] implicit val _apiTokenLoginRequest = jsonFormat2(LoginRequest)
  private[tokenrest] implicit val _apiTokenLoginResponse = jsonFormat3(LoginResponse)
}

trait TokenRestApi extends JsonMarshallers with SessionAuthentication with SimpleRoutingApp
{
  def restServiceName: String
  implicit def system: ActorSystem
  implicit def ec: ExecutionContext
  def log: LoggingAdapter
  def authorize:(String, String) => Future[Boolean]
  def privateRoute:String => Route
  def viewRoute:Route

  lazy val restSettings = TokenRest.createSettings(system, restServiceName)
  implicit lazy val timeout = Timeout(restSettings.timeout)

  implicit def remoteAddressToString(ra:RemoteAddress): String = ra.toString
  implicit def genericToOption[U](u:U): Option[U] =Option(u)

  implicit def myExceptionHandler(implicit log: LoggingContext) = {
    ExceptionHandler {
      case ex: Throwable =>
        requestUri { uri =>
          log.error("{}: {} - {}", uri, ex.getClass.getSimpleName, ex.getMessage)
          if(log.isDebugEnabled)
            ex.printStackTrace()
          complete(StatusCodes.InternalServerError -> FailureApiResponse(messages = List(ex.getMessage)).toJson.prettyPrint)
        }
    }
  }

  lazy val pingRoute = path("ping") {
    pathEnd {
      get {
        complete {
          new SimpleDateFormat("dd-mm-yyyy hh:mm:ss.SSS").format(new Date())
        }
      }
    }
  }

  lazy val loginRoute = pathPrefix("login") {
    pathEnd {
      clientIP { ip =>
        entity(as[LoginRequest]) { login =>
          (post | put) {
            complete {
              (system.actorOf(Props(new LoginWorker(authorize)), s"LoginWorker-${UUID.randomUUID()}") ? MessagingV1.Login(login.username, login.password, ip.toString())).map {
                case MessagingV1.LoginSuccessful(sessionId) =>
                  StatusCodes.OK -> LoginResponse(sessionId, true, None).toJson.prettyPrint
                case MessagingV1.LoginFailed =>
                  StatusCodes.Unauthorized -> LoginResponse(None, false, List("Login failed")).toJson.prettyPrint
              }
            }
          }
        }
      }
    }
  }

  lazy val sessionRoute = pathPrefix("private") {
    requestUri { url =>
      headerValueByName("session") { sessionId =>
        clientIP { ip =>
          clientSession(sessionId, ip.toString()) { username =>
            privateRoute(username)
          }
        }
      }
    }
  }

  lazy val routeDirectives: Route =
  {
    pathPrefix("service") {
      respondWithMediaType(MediaTypes.`application/json`) {
        handleExceptions(myExceptionHandler)
        {
          pingRoute ~ loginRoute ~ sessionRoute
        }
      }
    } ~ viewRoute
  }


  def initialize() = {
    log.info(s"Loading rest api with $restSettings")
    Try(Session.startShardRegion(system, restSettings.sessionTimeout))
    startServer(interface = restSettings.interface, port = restSettings.port) { routeDirectives }
  }
}

