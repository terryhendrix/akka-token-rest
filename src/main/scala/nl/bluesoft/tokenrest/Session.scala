package nl.bluesoft.tokenrest

import akka.persistence._
import akka.actor._
import akka.util.Timeout
import scala.concurrent.duration._
import akka.contrib.pattern.{ClusterSharding, ShardRegion}
import akka.persistence.SaveSnapshotFailure
import akka.persistence.SaveSnapshotSuccess
import akka.contrib.pattern.ShardRegion.Passivate
import akka.persistence.SnapshotOffer
import java.text.SimpleDateFormat
import nl.bluesoft.tokenrest.MessagingV1.CheckSession
import java.util.Date


object Session
{
  def props(sessionTimeout:FiniteDuration) = Props(new Session(sessionTimeout.toMillis))
  val numberOfShards = 100

  val idExtractor: ShardRegion.IdExtractor = {
    case msg @ MessagingV1.CreateSession(sessionId,_,_)   =>   (sessionId, msg)
    case msg @ MessagingV1.CheckSession(sessionId,_)      =>   (sessionId, msg)
    case msg @ MessagingV1.InvalidateSession(sessionId)   =>   (sessionId, msg)
  }

  val shardResolver: ShardRegion.ShardResolver = {
    case msg @ MessagingV1.CreateSession(sessionId,_,_)   =>   resolve(sessionId)
    case msg @ MessagingV1.CheckSession(sessionId,_)      =>   resolve(sessionId)
    case msg @ MessagingV1.InvalidateSession(sessionId)   =>   resolve(sessionId)
  }

  def startShardRegion(system:ActorSystem, sessionTimeout:FiniteDuration):Unit =
    ClusterSharding(system).start (
      typeName = "Session",
      entryProps = Option(Session.props(sessionTimeout)),
      idExtractor = Session.idExtractor,
      shardResolver = Session.shardResolver
    )

  private def resolve(key:String):String = (key.hashCode % numberOfShards).toString
}

sealed case class SessionData(sessionId:String, username:String, ip:String, timestamp:Long)
import Session._
class Session(maxSessionDuration:Long) extends PersistentActor with ActorLogging
{
  val sessionId                 = context.self.path.toString.split("/").last
  override def persistenceId    = s"Session-$sessionId"
  def dateFormat                = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss")
  var state:Option[SessionData] = None
  implicit val timeout          = Timeout(3 seconds)
  implicit val ec              = context.system.dispatcher
  implicit def sessionDataToSessionDataOption(sd:SessionData) = Option(sd)

  context.setReceiveTimeout(5 seconds)

  override def receiveCommand: Receive = {
    case event: MessagingV1.CreateSession       =>  persist(event)(create)
    case event: MessagingV1.CheckSession        =>  checkSession(event)
    case event: MessagingV1.InvalidateSession   =>  persist(event)(invalidate)
    case MessagingV1.Stop                       =>  context.stop(self); log.debug(s"Pacifying")
    case ReceiveTimeout                         =>  context.parent ! Passivate(MessagingV1.Stop)
    case SaveSnapshotSuccess(meta)              =>  log.debug(s"Snapshot created")
    case SaveSnapshotFailure(meta, ex)          =>  log.error(s"Failed to save snapshot", ex)
    case any                                    =>  log.warning(s"Dropping message $any")
  }

  override def receiveRecover: Receive = {
    case RecoveryCompleted                                =>  log.debug("Recovery completed")
    case event:MessagingV1.CreateSession                  =>  create(event)
    case event:MessagingV1.InvalidateSession              =>  invalidate(event)
    case SnapshotOffer(meta, snapshot:Some[SessionData])  =>  state = snapshot; log.debug(s"Recovering session.")
    case SnapshotOffer(meta, None)                        =>  state = None; log.debug(s"Session is invalid.")
  }

  def create(event:MessagingV1.CreateSession)         = {
    log.info(s"Creating session for ${event.username}")
    changeTo(SessionData(event.sessionId, event.username, event.ip, System.currentTimeMillis))
  }
  def invalidate(event:MessagingV1.InvalidateSession) = {
    log.info(s"Invalidating session for ${state.map(_.username).getOrElse("<no username>")}")
    changeTo(None)
  }

  def changeTo(state:Option[SessionData]):Unit = {
    this.state = state
    if(!recoveryRunning)
      saveSnapshot(state)
  }

  def checkSession(event:CheckSession):Unit = {
    log.debug("Checking session")
    state map { sd =>
      if(isValid(event, sd)) {
        state = sd.copy(timestamp = System.currentTimeMillis)
        sender ! MessagingV1.BelongsToUser(sd.username)
      }
      else
      {
        if(notTheSameIp(event, sd))
          log.warning(s"Possible session hi-jack attempt from ${sd.ip}")
        else
          log.info("Session has expired!")
        self ! MessagingV1.InvalidateSession(sd.sessionId)
        sender ! MessagingV1.InvalidSession
      }
    } getOrElse {
      log.info("Session does not exist")
      sender ! MessagingV1.InvalidSession
    }
  }

  def isValid(event:CheckSession, sd:SessionData) = isThisSession(event,sd) && isTheSameIp(event,sd) && hasNotExpired(event, sd)
  def isThisSession(event:CheckSession, sd:SessionData) = sd.sessionId == event.sessionId
  def isTheSameIp(event:CheckSession, sd:SessionData) = sd.ip == event.ip
  def notTheSameIp(event:CheckSession, sd:SessionData) = !isTheSameIp(event, sd)
  def hasNotExpired(event:CheckSession, sd:SessionData) = sd.timestamp + maxSessionDuration >= System.currentTimeMillis()
  
}

trait SessionRegion
{
  def system:ActorSystem
  lazy val sessionRegion:ActorRef = ClusterSharding(system).shardRegion("Session")
}


