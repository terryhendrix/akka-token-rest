# TokenRestApi #

The TokenRestApi lets users log in to your system based on a session hash.

Available calls:

1. GET: /service/ping

2. POST: /service/login {"username":"terry", "password":"1234"}

3. GET: /service/private/<yourwebservice> 

## How to use ##

The TokenRestApi trait must be mixed in your own trait, class or object. The following is an example of a trait.

```
#!scala
trait MyRestApi extends TokenRestApi
{
  override def restServiceName: String = "my-rest-api"
  override implicit val ec = system.dispatcher
  override def privateRoute = { username =>
    pathPrefix("test") {
      complete {
        s"Secret stuff for $username"
      }
    }
  }

  def authorize:(String, String) => Future[Boolean] = {
    (username, password) => Future{ username == "admin" && password == "1234"}
  }
}
```

Only the basic items are currently configurable in application.conf. See below:
```
#!scala
rest
{
    my-rest-api
    {
        interface = "0.0.0.0"
        port = 7777
        timeout = 3 seconds
    }
}

```

## How it works ##

![IMAGE](https://bytebucket.org/terryhendrix/akkatokenrestapi/raw/d2fb3097aa28f26ebe094606d10a6c8d4d1a7807/docs/TokenRestApiActorSystem.png)